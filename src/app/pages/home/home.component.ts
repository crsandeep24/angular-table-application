import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
//import { ColumnMode } from 'projects/swimlane/ngx-datatable/src/public-api';
import jsonData from '../../datafiles/sample_data.json';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  sampleData:any;
  selected = [];
  rowsLimit = 10;

  constructor() { }

  ngOnInit() {
    this.sampleData = jsonData;
  }

  onChange(targetValue) {
    this.rowsLimit = targetValue;
}
}
